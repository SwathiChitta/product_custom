{
	"name" : "Product Customization",
	"version" : "1.0",
	"author" : "Anipr",
	"website" : "www.anipr.in",
	"category" : "Tools",
	"depends" : ["sale"],
	"description" : "Product Details",
	"update_xml" : ["custom_products_view.xml"],
	"installable": True,
	"application" :True,
}