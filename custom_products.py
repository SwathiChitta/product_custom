from openerp.osv import fields, osv
from openerp import tools, api

class Customer_products(osv.osv):
	_inherit = "product.template"

	_columns = {
		'carton_quantity'  : fields.integer('Carton Quantity'),
		'bottle_price'  : fields.float('Single Bottle Price'),
	}

	
	@api.onchange('carton_quantity','list_price')
	def _calculate_amount(self):
		if self.carton_quantity and self.list_price:
			self.bottle_price = self.list_price / self.carton_quantity
Customer_products()

